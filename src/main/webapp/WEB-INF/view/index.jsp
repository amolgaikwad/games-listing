<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script>
    $(document).ready(function() {
        $('#gamesList').DataTable();
    } );
    </script>
</head>
<body>
    <div>
        <div>
            <h1>Games Listing</h1>

        </div>
        <table id="gamesList" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>URL</th>
                        <th>Platform</th>
                        <th>Score</th>
                        <th>Genre</th>
                        <th>Editors Choice</th>
                        <th>Release Year</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                                            <th>Title</th>
                                            <th>URL</th>
                                            <th>Platform</th>
                                            <th>Score</th>
                                            <th>Genre</th>
                                            <th>Editors Choice</th>
                                            <th>Release Year</th>
                                        </tr>
                </tfoot>
                <tbody>
                <c:forEach var="game" items="${games}">
                    <tr>
                        <td>${game.title}</td>
                        <td>${game.url}</td>
                        <td>${game.platform.name}</td>
                        <td>${game.score}</td>
                        <td>${game.genre}</td>
                        <td>${game.editorsChoice.choice}</td>
                        <td>${game.releaseYear}</td>
                    </tr>
                    <br/>
                </c:forEach>
                </tbody>
         </table>
    </div>
</body>
</html>