package games.listing;

import games.listing.domain.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GamesListingService {

    @Autowired
    private GamesRepository gamesRepository;

    public List<Game> retrieve() {

        return gamesRepository.retrieve();
    }
}
