create table Game(id int(10),
                  title varchar(100),
                  url varchar(100),
                  platform varchar(50),
                  score double(10),
                  genre varchar(100),
                  editorsChoice char(1),
                  releaseYear int(4));