package games.listing.domain;

import org.apache.commons.lang3.StringUtils;

public enum Platform {
    PLATSTATION_VITA("PlayStation Vita"), IPAD("iPad"), PC("PC");

    private String name;

    Platform(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Platform getPlatformBy(String name) {
        Platform platform = null;
        for (Platform tempPlatform : Platform.values()) {
            if (StringUtils.equalsIgnoreCase(tempPlatform.getName(), name)) {
                return platform;
            }
        }
        return platform;
    }
}
