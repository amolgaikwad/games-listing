package games.listing;

import games.listing.domain.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@Controller
public class GamesListingController {

    @Autowired
    private GamesListingService gamesListingService;

    @RequestMapping("/")
    public String showAllGames(Map<String, Object> model) {
        List<Game> games = gamesListingService.retrieve();
        model.put("games", games);
        return "index";
    }

}
