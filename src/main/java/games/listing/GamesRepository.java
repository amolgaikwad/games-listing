package games.listing;

import games.listing.domain.EditorsChoice;
import games.listing.domain.Game;
import games.listing.domain.Platform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class GamesRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Game> retrieve() {
        return jdbcTemplate.query("select * from game", new GameRowMapper());
    }

    private class GameRowMapper implements RowMapper<Game> {

        @Override
        public Game mapRow(ResultSet resultSet, int i) throws SQLException {
            Game game = new Game();
            game.setTitle(resultSet.getString("title"));
            game.setUrl(resultSet.getString("url"));
            game.setPlatform(Platform.getPlatformBy(resultSet.getString("platform")));
            game.setScore(resultSet.getDouble("score"));
            game.setReleaseYear(resultSet.getInt("releaseYear"));
            game.setEditorsChoice(EditorsChoice.getEditorsChoiceBy(resultSet.getString("editorsChoice").charAt(0)));
            return game;
        }
    }

}
