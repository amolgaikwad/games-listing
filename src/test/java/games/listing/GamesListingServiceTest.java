package games.listing;

import games.listing.domain.Game;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class GamesListingServiceTest {

    @InjectMocks
    private GamesListingService gamesListingService;

    @Mock
    private GamesRepository gamesRepository;

    @Test
    public void shouldGetGamesList() {
        Mockito.when(gamesRepository.retrieve()).thenReturn(createFakeGames());
        assertTrue(CollectionUtils.isNotEmpty(gamesListingService.retrieve()));
    }

    private List<Game> createFakeGames() {
        List<Game> games = new ArrayList<Game>();
        Game game = new Game();
        game.setTitle("SomeTitle");

        games.add(game);

        return games;
    }

}