package games.listing.domain;

public class Game {
    private String title;
    private String url;
    private Platform platform;
    private Double score;
    //Can be mutliple .. so List of genre enums?
    private String genre;
    private EditorsChoice editorsChoice;
    private Integer releaseYear;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public EditorsChoice getEditorsChoice() {
        return editorsChoice;
    }

    public void setEditorsChoice(EditorsChoice editorsChoice) {
        this.editorsChoice = editorsChoice;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }
}
