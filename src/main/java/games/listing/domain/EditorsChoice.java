package games.listing.domain;

public enum EditorsChoice {
    YES('Y'), NO('N');

    public char getChoice() {
        return choice;
    }

    private char choice;

    EditorsChoice(char choice) {
        this.choice = choice;
    }

    public static EditorsChoice getEditorsChoiceBy(char choice) {
        if (choice == 'Y') {
            return YES;
        } else {
            return NO;
        }
    }
}
